ARG CONTAINER_NAME=alpine:latest
FROM $CONTAINER_NAME

ARG JULIA_VERSION=1.5.3
ARG JULIA_MEASURE_VERSION=1.5
ARG C_LIB=musl

ENV JULIA_PATH /juliasrc
ENV PATH $PATH:$JULIA_PATH/bin
RUN apk update &&\
	apk upgrade && \
	apk add --no-cache curl \
		gcc \
		g++ \
		musl-dev \
		libffi-dev \
		ffmpeg-dev \
		gnu-libiconv \
		fdk-aac \
		python3-dev \
		py3-pip \
		ipython \
		tar &&\
	mkdir -p $JULIA_PATH &&\
	cd $JULIA_PATH &&\
	curl -L https://julialang-s3.julialang.org/bin/${C_LIB}/x64/${JULIA_MEASURE_VERSION}/julia-${JULIA_VERSION}-${C_LIB}-x86_64.tar.gz \
	| tar zx -C $JULIA_PATH --strip-components 1 &&\
	curl -L "https://github.com/JuliaInterop/ZMQBuilder/releases/download/v4.2.5%2B6/ZMQ.x86_64-linux-musl.tar.gz" \
	| tar zx -C /usr/  &&\
	ln -sf /usr/lib/libbz2.so.1.0.8 /usr/lib/libbz2.so.1.0 &&\
	ln -sf /usr/lib/libfdk-aac.so.2.0.1 /usr/lib/libfdk-aac.so.1 &&\
	ln -sf /usr/lib/libx264.so.157 /usr/lib/libx264.so.161 &&\
	ln -sf /usr/lib/libx265.so.188 /usr/lib/libx265.so.169 &&\
	pip install jupyter &&\
	julia -e 'import Pkg; \
		Pkg.update() ;\
		Pkg.add("Plots") ;\
 		Pkg.add("CSV") ;\
 		Pkg.add("PhysicalConstants") ;\
		Pkg.add("Unitful") ;\
		Pkg.add("Measurements") ;\
		Pkg.add("DataFrames") ;\
		Pkg.add("IJulia") ;\
		Pkg.precompile()' 
